# TBZ Cloud Native Bootcamp

#### 1. Vagrant

##### Zuerst an einem Ort (mit rechten) ein neues Verzeichnis für Vagrantfile usw. erstellen z.B. /home/nginx.

`vagrant init ubuntu/xenial64 —minimal` _Erstellt ein minimales Vagrantfile im aktuellen Ordner für eine normale Ubuntu Xenial VM._

`vagrant up` _Startet die Vagrant VM, beim ersten mal geht länger da die VM heruntergeladen werden muss._

`vagrant halt` _Stoppt die Vagrant VM._

`touch provision.sh` _Das Provision Script erstellen_

<br>

###### Inhalt des Provision File:

`apt-get update -y` _Repository aktualisieren_

`apt-get install nginx -y` _Nginx installieren_

`sudo rm -rf /var/www/html` _Den Default Ordner für Webcontent löschen._

`sudo ln -s /vagrant/www /var/www/html` _Symbolischer Link von der Webcontent Location zum Vagrant synced Folder auf welchem sich der Webcontent befindet._

`service nginx start` _Nginx Service starten_

<br>

##### Vagrant File kann nun angepasst werden. Nachfolgende Einträge können hinzugefügt werden.

`config.vm.hostname = „HOSTNAME“` _Setzt der VM den angegebenen Hostname._

`config.vm.provision „shell“, path: „provision.sh“ -> hinterlegt das provision file` _Definiert den Pfad des Provision Scirpt._

`config.vm.network „forwarded_port“, guest: 80, host: 8080, id: „HOSTNAME“` _Portweiterleitung, dass der Webserver über den Port 8080 erreichbar ist._

<br>

##### Anhand dieses Provision File und Vagrantfile sollte nun die VM immer von neuem mit dem selben Webcontent erstellt werden.

`vagrant reload` _Führt einen vagrant halt und wieder einen vagrant up aus. Kann bei Anpassungen am Vagrantfile verwendet werden._

`vagrant provision` _Existiert das provision.sh beim ersten vagrant up nicht, kann dieses mit diesem Befehl manuell ausgeführt werden._